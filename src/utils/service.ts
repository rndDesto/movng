import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FetchService {
  constructor(private http: HttpClient) { }
  
  bearerAuth = 'eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJiNjU5Nzg5YjAwZjEzNzVjODU5YzEzODY3ODMwZjU3NCIsInN1YiI6IjY0YTYzNjg3MDdmYWEyMDBjN2ViY2ViZSIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.c1n7TJE4fkv__wvG_A8mxzSAMin8g81tBfYmNY9fUZI'


  getAll(baseUrl: string, token: string): Promise<any> {
    const headers = new HttpHeaders().set('Authorization', 'Bearer ' + token);
    return this.http.get<{ results: any[] }>(baseUrl, { headers }).toPromise();
  }


  fetchDataFromUrls():Promise<any>  {
    const headers = new HttpHeaders().set('Authorization', 'Bearer ' + this.bearerAuth);
    const urls = [
      'https://api.themoviedb.org/3/movie/top_rated',
      'https://api.themoviedb.org/3/movie/popular',
      'https://api.themoviedb.org/3/movie/upcoming'
    ];

    const requests = urls.map(url => this.http.get(url, { headers }).toPromise());

    return Promise.all(requests)
      .then((responses: any[]) => {
        return responses
      })
      .catch(error => {
        return error
      });
  }


  
}
