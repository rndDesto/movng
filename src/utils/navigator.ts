import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class NavigatorUtil {
  constructor(private router: Router) { }

  handleDetailMovie(id: string): void {
    this.router.navigate([`/detail/${id}`]);
  }
}