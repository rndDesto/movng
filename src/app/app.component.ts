import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'ngpwa';
  nav=false


  constructor(private route: Router) { }

  ngOnInit(): void {
    console.log("ccc = ", this.route)

    this.route.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.handleRouterChange(event.url);
      }
    });
  }

  handleRouterChange(url: string): void {
    const splitValue = url.split('/')[1];
    const hideMenu = ['latest','detail']
    this.nav = !hideMenu.includes(splitValue)
  }
}
