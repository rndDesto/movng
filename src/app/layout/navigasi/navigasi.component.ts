import { Component } from '@angular/core';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import { AppRoutingModule } from 'src/app/app-routing.module';

@Component({
  selector: 'app-navigasi',
  templateUrl: './navigasi.component.html',
  styleUrls: ['./navigasi.component.css'],
  standalone: true,
  imports: [MatToolbarModule, MatIconModule, AppRoutingModule],
})
export class NavigasiComponent {

}
