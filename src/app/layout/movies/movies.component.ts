import { Component, OnInit } from '@angular/core';
import { FetchService } from 'src/utils/service';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
// import { MovieDetailComponent } from 'src/app/components/movie-detail/movie-detail.component';
// import { MatSnackBar } from '@angular/material/snack-bar';
import { NavigatorUtil } from 'src/utils/navigator';


type allMovieModels = {
  success:boolean,
  data:Record<any,any>
}


@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})

export class MoviesComponent implements OnInit{
  mainTitle={
    popular : "Paling Populer",
    rated:'top rated',
    upcoming:'Akan Datang'

  }
  
  isLoading:boolean = false
  allMovies: allMovieModels | null = null;


  constructor(
    private useFetch: FetchService,
    // private bottomSheet: MatBottomSheet,
    // private snackBar: MatSnackBar,
    private nav:NavigatorUtil
    ) { }

  ngOnInit(): void {
    this.homeMovies()

  }

  async movieService(apiUrl:string){
    this.isLoading = true;
      try {
        const response = await this.useFetch.getAll(apiUrl, this.useFetch.bearerAuth);
        return {
          success:true,
          ...response
        };
      } catch (err) {
        return err
      }
      finally {
        this.isLoading = false;
      }
  }


  async homeMovies(): Promise<void> {
    this.isLoading = true;
    try {
      const response = await this.useFetch.fetchDataFromUrls()
      this.allMovies = {
        success:true,
        data:{
          ...response
        }
      }

    } catch (err) {
      console.log('eerr = ', err)
    }
    finally {
      this.isLoading = false;
    }
  }

  

  async getDetailMovie(movie:any): Promise<void> {
    this.nav.handleDetailMovie(movie.id);
    // const detailUrl = `https://api.themoviedb.org/3/movie/${movie.id}`
    // const detailMovie = await this.movieService(detailUrl)
    // if(detailMovie.success){
    //   this.bottomSheet.open(MovieDetailComponent,{data:{...detailMovie}})
    // }
    // else{
    //   this.snackBar.open(detailMovie.message, 'Tutup', {
    //     duration: 2000,
    //   });
    // }
  }
}
