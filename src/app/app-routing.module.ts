import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetailComponent } from './pages/detail/detail.component';
import { FavoriteComponent } from './pages/favorite/favorite.component';
import { HomeComponent } from './pages/home/home.component';
import { LatestComponent } from './pages/latest/latest.component';
import { SearchComponent } from './pages/search/search.component';
import { SignalpilotComponent } from './pages/signalpilot/signalpilot.component';

const routes: Routes = [
  { 
    path: '', 
    redirectTo: 'home', 
    pathMatch: 'full',
    data: {
    showMenu: true
  }},
  {
    path: 'home',
    component: HomeComponent,
    data: {
      showMenu: true
    }
  },
  {
    path: 'search', 
    component: SearchComponent, 
    data: {
      showMenu: true
    }
  },
  {
    path: 'favorite', 
    component: FavoriteComponent, 
    data: {
      showMenu: true
    }
  },
  {
    path: 'latest', 
    component: LatestComponent, 
    data: {
      showMenu: false
    }
  },
  {
    path: 'signalpilot', 
    component: SignalpilotComponent, 
    data: {
      showMenu: false
    }
  },
  {
    path: 'detail/:id', 
    component: DetailComponent, 
    data: {
      showMenu: true
    }
  },
  { path: '**', redirectTo: '/home' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
