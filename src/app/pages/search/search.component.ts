import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavigatorUtil } from 'src/utils/navigator';
import { FetchService } from 'src/utils/service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  debounceTimer: any;
  searchValue: string = '';
  data:any = [];
  isLoading:boolean = false;
  mainTitle = 'Popular Movie'

  constructor(
    private useFetch: FetchService,
    private nav:NavigatorUtil,
    private router: Router,
    private route: ActivatedRoute ) { }

    ngOnInit() {
      this.route.queryParams.subscribe(params => {
        this.searchValue = params['q'] || '';
        params['q'] && this.updateSearchResults(params['q']);
      });
    }


  async movieService(apiUrl:string){
    this.isLoading = true;
      try {
        const response = await this.useFetch.getAll(apiUrl, this.useFetch.bearerAuth);
        return {
          success:true,
          ...response
        };
      } catch (err) {
        return err
      }
      finally {
        this.isLoading = false;
      }
  }

  onSearch(event: Event): void {
    const inputValue = (event.target as HTMLInputElement).value;
    clearTimeout(this.debounceTimer);

    this.debounceTimer = setTimeout(() => {
      this.updateSearchResults(inputValue);
      
      this.router.navigate(['/search'], { queryParams: { q: this.searchValue } });
    }, 500);
  }

  async updateSearchResults(search: string): Promise<void> {
    const url = `https://api.themoviedb.org/3/search/movie?query=${search}`
    this.data = await this.movieService(url)
  }


  getDetailMovie(movie:any): void {
    this.nav.handleDetailMovie(movie.id);
  }

}
