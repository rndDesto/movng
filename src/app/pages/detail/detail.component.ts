import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FetchService } from 'src/utils/service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
  itemId: string | null = '';
  detailMovie: any = [];
  similarMovie: any = [];
  creditMovie: any = [];
  recomendedMovie: any = [];
  isLoading: boolean = false;
  mainTitle = 'Latest Movie'

  constructor(
    private route: ActivatedRoute,
    private useFetch: FetchService,
    private location: Location) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.itemId = params.get('id');
      this.getDetailMovies(params.get('id'))
      this.getSimilarMovie(params.get('id'))
      this.getCreditMovie(params.get('id'))
      this.getRecomendationMovie(params.get('id'))
    });
  }

  async movieService(apiUrl: string) {
    this.isLoading = true;
    try {
      const response = await this.useFetch.getAll(apiUrl, this.useFetch.bearerAuth);
      return {
        success: true,
        ...response
      };
    } catch (err) {
      return err
    }
    finally {
      this.isLoading = false;
    }
  }


  async getDetailMovies(id: string | null): Promise<void> {
    const detailUrl = `https://api.themoviedb.org/3/movie/${id}`
    this.detailMovie = await await this.movieService(detailUrl)
  }

  async getSimilarMovie(id: string | null): Promise<void> {
    const similarUrl = `https://api.themoviedb.org/3/movie/${id}/similar`
    this.similarMovie = await await this.movieService(similarUrl)
  }

  async getCreditMovie(id: string | null): Promise<void> {
    const creditUrl = `https://api.themoviedb.org/3/movie/${id}/credits`
    this.creditMovie = await await this.movieService(creditUrl)
  }
  async getRecomendationMovie(id: string | null): Promise<void> {
    const recomendationUrl = `https://api.themoviedb.org/3/movie/${id}/recommendations`
    this.recomendedMovie = await await this.movieService(recomendationUrl)
  }

  handleBack(){
    this.location.back();
  }

}
