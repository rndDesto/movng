import { Component, OnInit } from '@angular/core';
import { NavigatorUtil } from 'src/utils/navigator';
import { FetchService } from 'src/utils/service';

@Component({
  selector: 'app-latest',
  templateUrl: './latest.component.html',
  styleUrls: ['./latest.component.css']
})
export class LatestComponent implements OnInit{
  data:any = [];
  isLoading:boolean = false;
  mainTitle = 'Latest Movie'

  constructor(
    private useFetch: FetchService,
    private nav:NavigatorUtil) { }

  ngOnInit(): void {
    this.getMovies()
  }
  
  async movieService(apiUrl:string){
    this.isLoading = true;
      try {
        const response = await this.useFetch.getAll(apiUrl, this.useFetch.bearerAuth);
        return {
          success:true,
          ...response
        };
      } catch (err) {
        return err
      }
      finally {
        this.isLoading = false;
      }
  }


  async getMovies(): Promise<void> {
    const url = 'https://api.themoviedb.org/3/discover/movie'
    this.data = await this.movieService(url)
  }

  getDetailMovie(movie:any): void {
    this.nav.handleDetailMovie(movie.id);
  }

}
