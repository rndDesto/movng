import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SignalpilotComponent } from './signalpilot.component';

describe('SignalpilotComponent', () => {
  let component: SignalpilotComponent;
  let fixture: ComponentFixture<SignalpilotComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SignalpilotComponent]
    });
    fixture = TestBed.createComponent(SignalpilotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
