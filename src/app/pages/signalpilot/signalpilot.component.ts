import { Component } from '@angular/core';

@Component({
  selector: 'app-signalpilot',
  templateUrl: './signalpilot.component.html',
  styleUrls: ['./signalpilot.component.css']
})
export class SignalpilotComponent {

  nama: string = 'John Doe';

  mantulgan(e:any){
    console.log("jaya jaya jaya = ", e.target.value)
  }

  mantap(e:any){
    this.nama = e.detail.value
    console.log("mantap = ",e.detail)
  }

}
