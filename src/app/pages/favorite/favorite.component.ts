import { Component, OnInit } from '@angular/core';
import { FetchService } from 'src/utils/service';

@Component({
  selector: 'app-favorite',
  templateUrl: './favorite.component.html',
  styleUrls: ['./favorite.component.css'],
})
export class FavoriteComponent implements OnInit {
  data: any = [];
  isLoading: boolean = false;
  mainTitle = 'Favorites';

  constructor(private useFetch: FetchService) {}
  ngOnInit(): void {
    this.getMoviesFav();
  }

  async movieService(apiUrl: string) {
    this.isLoading = true;
    try {
      const response = await this.useFetch.getAll(
        apiUrl,
        this.useFetch.bearerAuth
      );
      return {
        success: true,
        ...response,
      };
    } catch (err) {
      return err;
    } finally {
      this.isLoading = false;
    }
  }

  async getMoviesFav(): Promise<void> {
    const url = 'https://api.themoviedb.org/3/account/20112973/favorite/movies';
    this.data = await this.movieService(url);
  }
}
