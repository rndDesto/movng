import { CUSTOM_ELEMENTS_SCHEMA, NgModule, isDevMode } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MoviesComponent } from './layout/movies/movies.component';
import { HttpClientModule } from '@angular/common/http';
import { NavigasiComponent } from './layout/navigasi/navigasi.component';
import { HomeComponent } from './pages/home/home.component';
import { FavoriteComponent } from './pages/favorite/favorite.component';
import { SearchComponent } from './pages/search/search.component';
import { TitleMovieComponent } from './components/title-movie/title-movie.component';
import { MovieDetailComponent } from './components/movie-detail/movie-detail.component';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MovieCardComponent } from './components/movie-card/movie-card.component';
import { LatestComponent } from './pages/latest/latest.component';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatIconModule } from '@angular/material/icon';
import { FormsModule } from '@angular/forms';
import { DetailComponent } from './pages/detail/detail.component';
import { MovieActorComponent } from './components/movie-actor/movie-actor.component';
import {MatTabsModule} from '@angular/material/tabs';
// import 'mantulit';
// import '@litmtsel/core';
import '@sgnl/signal-ui';
import { SignalpilotComponent } from './pages/signalpilot/signalpilot.component';

@NgModule({
  declarations: [
    AppComponent,
    MoviesComponent,
    HomeComponent,
    FavoriteComponent,
    SearchComponent,
    MovieDetailComponent,
    LatestComponent,
    DetailComponent,
    MovieActorComponent,
    SignalpilotComponent,
  ],
  imports: [
    MatTabsModule,
    MatIconModule,
    TitleMovieComponent,
    MatBottomSheetModule,
    MatSnackBarModule,
    NavigasiComponent,
    MovieCardComponent,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: !isDevMode(),
      // Register the ServiceWorker as soon as the application is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: 'registerWhenStable:30000'
    }),
    HttpClientModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
