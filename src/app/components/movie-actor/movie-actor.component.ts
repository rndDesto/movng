import { Component, Injectable, Input } from '@angular/core';

@Component({
  selector: 'app-movie-actor',
  templateUrl: './movie-actor.component.html',
  styleUrls: ['./movie-actor.component.css']
})
@Injectable({
  providedIn: 'root'
})
export class MovieActorComponent {
  @Input() data?: any;

}
