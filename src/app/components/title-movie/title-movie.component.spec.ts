import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TitleMovieComponent } from './title-movie.component';

describe('TitleMovieComponent', () => {
  let component: TitleMovieComponent;
  let fixture: ComponentFixture<TitleMovieComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TitleMovieComponent]
    });
    fixture = TestBed.createComponent(TitleMovieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
