import { Component, Injectable, Input} from '@angular/core';
import { AppRoutingModule } from 'src/app/app-routing.module';

@Component({
  selector: 'app-title-movie',
  templateUrl: './title-movie.component.html',
  styleUrls: ['./title-movie.component.css'],
  imports: [AppRoutingModule],
  standalone:true
})
@Injectable({
  providedIn: 'root'
})
export class TitleMovieComponent {
  @Input() title?: string;

}
