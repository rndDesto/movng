import { Component, Injectable, Input } from '@angular/core';
import {MatCardModule} from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import {MatTooltipModule} from '@angular/material/tooltip';

@Component({
  selector: 'app-movie-card',
  templateUrl: './movie-card.component.html',
  styleUrls: ['./movie-card.component.css'],
  imports: [MatCardModule, MatIconModule, MatTooltipModule],
  standalone: true,
})
@Injectable({
  providedIn: 'root'
})
export class MovieCardComponent {
  @Input() data?: any;
}
