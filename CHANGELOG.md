

## 0.4.0 (2023-07-27)


### Features

* pilot signal ([fac5ad1](https://gitlab.com/rndDesto/movng/commit/fac5ad1e6fc0506377168c17ebcced6b6e44ea61))


### Bug Fixes

* rm space ([cf72779](https://gitlab.com/rndDesto/movng/commit/cf7277989fc607f9cb29578804ba3df3f79fe38f))

## 0.3.3 (2023-07-12)

## 0.3.2 (2023-07-12)

## 0.3.1 (2023-07-12)


### Bug Fixes

* fixing search ([8cbbf41](https://gitlab.com/rndDesto/movng/commit/8cbbf4112ce612eed996e833a73a33dea89c7317))

## 0.3.0 (2023-07-12)


### Features

* fitur cari dan detail ([8c3178c](https://gitlab.com/rndDesto/movng/commit/8c3178c35b261e2606c363f1c905d96b6964c259))


### Bug Fixes

* mantul gan ([b98e210](https://gitlab.com/rndDesto/movng/commit/b98e21047b3bc352a64439c8a201a7de3405bd26))

## 0.2.1 (2023-07-12)


### Bug Fixes

* format doc pretier ([c55cd30](https://gitlab.com/rndDesto/movng/commit/c55cd3043392d80ca3e1d596dd770ead1a3d1153))
* tambah elipsis ([640801f](https://gitlab.com/rndDesto/movng/commit/640801fd908b90ad7ccb8784f600ff62b484cfab))

## 0.2.0 (2023-07-12)


### Features

* halaman detal ([be9ec9a](https://gitlab.com/rndDesto/movng/commit/be9ec9a3c4347b5fb7894238c60d4349a50ba651))

## 0.1.0 (2023-07-12)


### Features

* **edit title:** ganti title ([5cc9dd3](https://gitlab.com/rndDesto/movng/commit/5cc9dd31463d824000aa8e535cfa613042924688))
* fav page ([c6b8cc4](https://gitlab.com/rndDesto/movng/commit/c6b8cc4cb26c9e0df99ebda65c409def0ee27732))
* search page ([a85f91d](https://gitlab.com/rndDesto/movng/commit/a85f91d86b37eca03caf7f74b724e3fc002e07ad))
* update code supaya bisa pake cz dan release-it ([017963e](https://gitlab.com/rndDesto/movng/commit/017963edfa1ff6db24c7f4b1901ae2045d6e6316))